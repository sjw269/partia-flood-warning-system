import floodsystem.geo as geo
from floodsystem.stationdata import build_station_list

"""Part 1 - using rivers_with_station to find the number of rivers being monitored
and to print the first 10 alphabetically"""

x = geo.rivers_with_station(build_station_list())
y = []
for i in x:
    y.append(i)
y.sort()
print(len(y))
print(y[:10])


"""Part 2 - using stations_by_river to produce lists of station names on the river"""

z = geo.stations_by_river(build_station_list())
a = z['River Aire']
b = z['River Cam']
c = z['River Thames']

d = []
for i in a:
    d.append(i.name)
d.sort()
print(d)
e = []
for i in b:
    e.append(i.name)
e.sort()
print(e)
f = []
for i in c:
    f.append(i.name)
f.sort()
print(f)

