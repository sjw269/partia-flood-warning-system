"""This file tests the functions contained in the geo module"""


from floodsystem.geo import stations_by_distance, hav, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

def test_stations_by_distance():
    """This will test that the tuple is created correctly, with distance being a float and greater than 0"""
    x = build_station_list()
    y = stations_by_distance(x, (0,0))
    for i in y:
        assert type(i[1]) == float and i[1] >= 0

def test_hav():
    """Checks the haversine function is outputting correctly"""
    x = hav(90,180)
    y = hav(45,90)
    assert round(x,6) == 0.5
    assert round(y,6) == 0.146447

def test_stations_within_radius():
    """This will test that the function only includes stations within the radius"""
    x = build_station_list()
    y = stations_within_radius(x, (52.2053,0.1218), 1)
    assert len(y) == 1 and y[0][0].name == "Cambridge Jesus Lock"

def test_rivers_with_station():
    """This will check that the names are of type string and that data entries exist"""
    x = build_station_list()
    y = rivers_with_station(x)
    for i in y:
        assert type(i) == str
    assert len(y) > 0

def test_stations_by_river():
    """Checks that a known river has a list of stations and 
    the type returned is the required dictionary"""
    x = build_station_list()
    y = stations_by_river(x)
    assert len(y['River Thames']) > 0
    assert type(y) == dict

def test_rivers_by_station_number():
    for station in build_station_list():
        if not isinstance(station, MonitoringStation):
            raise TypeError("Must have argument as a MonitoringStation Class object")
        else:
             pass

    
