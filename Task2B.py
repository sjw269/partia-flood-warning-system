from floodsystem.flood import stations_levels_over_threshold
from floodsystem.stationdata import build_station_list

def run():
    tol= 0.8
    stations = build_station_list()
    x = stations_levels_over_threshold(stations, tol)
    print(x)

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()




