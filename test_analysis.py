import floodsystem.analysis as a
import matplotlib.dates
import datetime

def test_polyfit():
    """This will test that the polyfit provides accurate results"""
    dates = [datetime.datetime(2020, 1, 1,), datetime.datetime(2020, 1, 2,), datetime.datetime(2020, 1, 3,), datetime.datetime(2020, 1, 4,), datetime.datetime(2020, 1, 5,)]
    x = matplotlib.dates.date2num(datetime.datetime(2020, 1, 6,))
    levels = [1, 2, 3, 4, 5]
    curve = a.polyfit(dates, levels, 4)
    poly = curve[0]
    assert round(poly(x - curve[1]),1) == 6
    assert curve[1] == matplotlib.dates.date2num(datetime.datetime(2020, 1, 1,))