import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from .datafetcher import fetch_measure_levels
import numpy as np
from .analysis import polyfit
import matplotlib

def plot_water_levels(station, dates, levels):
    if dates == None:
        return None
    elif levels == None:
        return None
    else:

        plt.plot(dates, levels, label = "{}".format(station.name))
        plt.xlabel("date")
        plt.ylabel("water level (m)")
        plt.xticks(rotation=45)
        plt.title(station.name)
        if station.typical_range_consistent() == True:
            y1= [station.typical_range[1]]*len(levels)
            y2= [station.typical_range[0]]*len(levels)
            plt.plot(dates, y1, label = "Typical high")
            plt.plot(dates, y2, label = "Typical low" )
        plt.legend()
        plt.tight_layout()
        #plt.ylim(0, 1+station.typical_range[1])
        return plt
 
def plot_water_levels_with_fit(station, dates, levels, p):
    """Uses the plot function above and combines it with polyfit to give a curve with the line of best fit added"""
    plot_water_levels(station[0], dates, levels)
    curve = polyfit(dates, levels, p)
    poly = curve[0]
    x = matplotlib.dates.date2num(dates)
    y = poly(x - x[0])
    plt.plot(x, y, label = "Best-fit curve")
    plt.legend()
    plt.show()
