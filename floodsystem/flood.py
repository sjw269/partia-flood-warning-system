from .stationdata import update_water_levels

def stations_levels_over_threshold(stations, tol):
    """Returns a list of tuples with (station, water level) when the water level is above the tolerance"""
    list_of_tuples = []
    relative_level = []
    #update_water_levels(stations)
    for station in stations:
        if station.relative_water_level() == None:
            pass
        else:
            relative_level = station.relative_water_level()
            if relative_level > tol:
                y = (station.name , relative_level)
                list_of_tuples.append(y)
    list_of_tuples.sort(key = lambda x:x[1], reverse = True)
    return list_of_tuples




def stations_highest_rel_level(stations, N):
    """Returns a list of the N stations with the highest relative level"""
    update_water_levels(stations)
    list_of_tuples = []
    relative_level = []
    for station in stations:
        if station.relative_water_level() == None:
            pass
        else:
            relative_level = station.relative_water_level()
            y = (station , relative_level)
            list_of_tuples.append(y)      
    list_of_tuples.sort(key = lambda x:x[1], reverse = True)
    stations_highest_rel_level = []
    for i in range(N):
        stations_highest_rel_level.append(list_of_tuples[i])
    return stations_highest_rel_level