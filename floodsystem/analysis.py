"""This submodule contains the polynomial best fit functions"""

import matplotlib.dates
import numpy as np

def polyfit(dates, level, p):
    """Returns a tuple containing the polynomial function and time shift"""
    x = matplotlib.dates.date2num(dates)
    if len(x) > 0 and len(level) > 0:
        d0 = x[0]
        for i in range(len(x)):
            x[i] = x[i] - d0
        y = level
        p_coeff = np.polyfit(x, y, p)
        poly = np.poly1d(p_coeff)
    
        return poly, d0