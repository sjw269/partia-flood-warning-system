# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
import math as m
from .stationdata import build_station_list


def stations_by_distance(stations, p):
    """This function calaculates the distance between 2 coordinates"""
    station_distances_list = []
    for station in stations:
        la = station.coord[0]
        lo = station.coord[1]
        a = p[0]
        b = p[1]

        distance = 12742 * m.asin(m.sqrt(hav(a, la) + m.cos(la * m.pi / 180) * m.cos(a * m.pi / 180) * hav(b, lo)))
        station_distances_list.append((station, distance))

    station_distances_list = sorted_by_key(station_distances_list,1)

    return station_distances_list

def hav(x,y):
    """This is the haversine function"""
    return 0.5 * (1 - m.cos((y - x) * m.pi / 180))

def stations_within_radius(stations, centre, r):
    """This will output a list of stations within the radius"""
    stations_in = []
    station_distances_list = stations_by_distance(stations, centre)
    for x in station_distances_list:
        if x[1] < r:
            stations_in.append(x)
        else:
            pass
    return stations_in

def rivers_with_station(stations):
    """This will return a set containing the river names that have monitoring stations on them"""
    x = set()
    for station in stations:
        if type(station.name) == None:
            pass
        else:
            x.add(station.river)
    return x

def stations_by_river(stations):
    """This will return a dictionary that maps river names and station objects"""
    river_dict = {}
    for station in stations:
        if station.river not in river_dict:
            river_dict[station.river] = [station]
        else:
            river_dict[station.river].append(station)
    return river_dict

def rivers_by_station_number(stations,N):
    """Returns the rivers with the greatest number of monitoring stations"""
    y=[]
    river_stations = stations_by_river(stations)
    num_stations_by_river = {}
    for river, names in river_stations.items():
        num_stations_by_river[river] = len(names)
    
    sorted_number_of_stations_per_river = sorted_by_key(num_stations_by_river.items(), 1, reverse = True)
    
    for river, n in sorted_number_of_stations_per_river:
        if n >= sorted_number_of_stations_per_river[N][1]:
            y.append((river, n))
    return y


