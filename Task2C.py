from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list


def run():
    N=10
    stations = build_station_list()
    x = stations_highest_rel_level(stations , N)
    y=[]
    for station, level in x:
        y.append((station.name, level))
    print(y)

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()