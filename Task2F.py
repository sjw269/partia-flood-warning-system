from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels_with_fit
from floodsystem.datafetcher import fetch_measure_levels
import datetime

stations = build_station_list()
highest_stations = stations_highest_rel_level(stations, 5)
dt = 2

for station in highest_stations:
    dates, levels = fetch_measure_levels(station[0].measure_id, dt=datetime.timedelta(days=dt))
    plot_water_levels_with_fit(station, dates, levels, 4)
    