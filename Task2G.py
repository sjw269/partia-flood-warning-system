from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
import datetime
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.flood import stations_levels_over_threshold

stations_build = build_station_list()
update_water_levels(stations_build)
dt = 2
severe_list = []
high_list = []
moderate_list = []
low_list = []
no_data_list = []
N = 10
stations = stations_highest_rel_level(stations_build, N)

while stations[-1][0].latest_level > 1.5 * stations[-1][0].typical_range[1]:
    N += 10
    stations = stations_highest_rel_level(stations_build, N)

print('Number of stations identified as at risk:')
print(N)
print('\n')

for station in stations:
    station = station[0]
    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
    if levels == None or len(levels) < 2 or station.latest_level == None or station.typical_range == None or station.town == None:
        if station.town == None:
            pass
        else:
            no_data_list.append(station.town)
    else:
        if station.town in severe_list or station.town in high_list or station.town in moderate_list or station.town in low_list:
            pass
        else:
            median = levels[len(levels) // 2]
            diff_upper = levels[-1] - median
            diff_lower = median - levels[0]
            if station.latest_level > 1.5 * station.typical_range[1]:
                if levels[-1] > levels[0]:
                    if diff_upper > diff_lower:
                        severe_list.append(station.town)
                    else:
                        high_list.append(station.town)
                else:
                    moderate_list.append(station.town)
            else:
                low_list.append(station.town)

severe_list.sort()
high_list.sort()
moderate_list.sort()
low_list.sort()
no_data_list.sort()

print("Severe: \n")
print(severe_list)
print("\n")
print("High: \n")
print(high_list)
print("\n")
print("Moderate: \n")
print(moderate_list)
print("\n")
print("Low: \n")
print(low_list)
print("\n")
print("No data: \n")
print(no_data_list)
