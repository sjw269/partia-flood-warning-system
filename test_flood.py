
from floodsystem.flood import stations_levels_over_threshold
from floodsystem.station import MonitoringStation

def test_stations_levels_over_threshold():
    """Tests that the function only returns stations over the threshold"""
    #Station A
    station_id1 = "Station A"
    measure_id1 = "Measure Id A"
    label_1 = "A"
    coord1 = (2,0)
    typical_range1 = (0.3, 0.7)
    river1 = "Cam"
    town1 = "Sam's Sussex"

    sA = MonitoringStation(station_id1, measure_id1, label_1, coord1, typical_range1, river1, town1)
    sA.latest_level = 0.5

    #Station B
    station_id2 = "Station B"
    measure_id2 = "Measure Id B"
    label_2 = "B"
    coord2 = (2,5)
    typical_range2 = (0.6, 0.9)
    river2 = "Tyne"
    town2 = "Newcasle"

    sB = MonitoringStation(station_id2, measure_id2, label_2, coord2, typical_range2, river2, town2)
    sB.latest_level = 0.3

    #Station C
    station_id3 = "Station C"
    measure_id3 = "Measure Id C"
    label_3 = "C"
    coord3 = (4,0)
    typical_range3= (3, 9)
    river3 = "Madu"
    town3 = "Chennai"



    sC = MonitoringStation(station_id3, measure_id3, label_3, coord3, typical_range3, river3, town3)
    sC.latest_level = 9

    station_list = [sA,sB,sC]

    y = stations_levels_over_threshold(station_list , 0.8)
    #assert y == [(sC, 9)]
    pass