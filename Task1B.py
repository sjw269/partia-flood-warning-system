"""This program finds the 10 nearest and furthest stations from p
and displays them"""

import floodsystem.geo as geo
import floodsystem.stationdata as st

p = (52.2053, 0.1218)
x = geo.stations_by_distance(st.build_station_list(), p)
closest_10 = []
furthest_10 = []

for i in range(10):
    closest_10.append((x[i][0].name,x[i][0].town,x[i][1]))
print("These are the 10 closest stations: ", closest_10)

for i in range(-10,0):
    furthest_10.append((x[i][0].name,x[i][0].town,x[i][1]))
print("These are the 10 furthest stations: ", furthest_10)