import floodsystem.geo as geo
from floodsystem.stationdata import build_station_list

p = (52.2053, 0.1218)
r = 10
x = geo.stations_within_radius(build_station_list(), p, r)

station_names = []
for i in x:
    station_names.append(i[0].name)
station_names.sort()
print("These are the stations within radius {}km:\n".format(r))
print(station_names)

