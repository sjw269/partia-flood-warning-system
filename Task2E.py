import floodsystem.plot
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import matplotlib.pyplot as plt
import datetime
import numpy as np
stations = build_station_list()
highest_stations = stations_highest_rel_level(stations, 5)


print(highest_stations)
for station in highest_stations:
    dt = 10
    #dates, levels = [], []
    dates, levels = fetch_measure_levels(station[0].measure_id, dt=datetime.timedelta(days=dt))
    #print(station[0].name)
    #print("----------------------------------------- {} ------------------------------------------------" .format(dates))
    #print("----------------------------------------- {} ------------------------------------------------" .format(levels))
    if dates == None:
        print("none")
    print(len(dates))
    print(len(levels))
    #print(station[0].typical_range[0])
    #print(station[0].typical_range[1])
    #if dates == None:
        #pass
        #if levels == None:
            #pass
    plot_water_levels(station[0], dates, levels)
    plt.show()

        
